//
//  SchoolDetailsModel.swift
//  20230317-WenyuZhao-NYCSchools
//
//  Created by Wenyu Zhao on 3/17/23.
//
//  Created a model of School Detail items

import Foundation

typealias SchoolDetails = [SchoolDetail]

// School Detail Model
struct SchoolDetail: Decodable, Hashable {
    var dbn: String?
    var school_name: String?
    var num_of_sat_test_takers: String?
    var sat_critical_reading_avg_score: String?
    var sat_math_avg_score: String?
    var sat_writing_avg_score: String?
}
