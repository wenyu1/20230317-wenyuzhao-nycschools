//
//  SchoolDetailTableViewCell.swift
//  20230317-WenyuZhao-NYCSchools
//
//  Created by Wenyu Zhao on 3/17/23.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var numOfSatTakers: UILabel!
    @IBOutlet weak var mathAvgScore: UILabel!
    @IBOutlet weak var readingAvgScore: UILabel!
    @IBOutlet weak var writingAvgScore: UILabel!
    
    
    lazy var schoolDetailViewModel = {
        SchoolDetailsViewModel()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeSchoolDetails()
    }
    
    func initializeSchoolDetails() {
        schoolDetailViewModel.getSchoolDetails()
        schoolDetailViewModel.reloadSchoolData = { [weak self] in
            DispatchQueue.main.async {
                self?.setSchoolValues()
            }
        }
        schoolDetailViewModel.showAlertView = {
            let alert = UIAlertController(title: "SAT Scores Not Available", message: "The SAT scores of this school is currently unavailable", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                    case .default:
                    self.navigationController?.popViewController(animated: true)
                    
                    case .cancel:
                    print("cancel")
                    
                    case .destructive:
                    print("destructive")
                    
                @unknown default:
                    print("Error")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // Set the values to the UI
    func setSchoolValues() {
        self.schoolName.text = self.schoolDetailViewModel.schoolDetail.school_name
        self.numOfSatTakers.text = self.schoolDetailViewModel.schoolDetail.num_of_sat_test_takers
        self.mathAvgScore.text = self.schoolDetailViewModel.schoolDetail.sat_math_avg_score
        self.readingAvgScore.text = self.schoolDetailViewModel.schoolDetail.sat_critical_reading_avg_score
        self.writingAvgScore.text = self.schoolDetailViewModel.schoolDetail.sat_writing_avg_score
    }
}

