//
//  SchoolDetailViewModel.swift
//  20230317-WenyuZhao-NYCSchools
//
//  Created by Wenyu Zhao on 3/17/23.
//
//   View Model of the School Detail item

import Foundation
import Combine

class SchoolDetailsViewModel: NSObject {
    
    private var cancellables = Set<AnyCancellable>()
    var reloadSchoolData: (() -> Void)?
    var dbn: String?
    var schoolData = SchoolDetails()
    var schoolDetail = SchoolDetail()
    var showAlertView: (() -> Void)?
    
    // Retrieve data from the url
    func getSchoolDetails() {
        let schoolDetailUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        NetworkManager.shared.getData(url: schoolDetailUrl,type: SchoolDetail.self)
            .sink { completion in
                switch completion {
                case .failure(let err):
                    print("Error is \(err.localizedDescription)")
                case .finished:
                    print("Finished")
                }
            }
    receiveValue: { [weak self] usersData in
        self?.schoolData = usersData
        if let details = self?.schoolData {
            for schoolItem in details where schoolItem.dbn == self?.dbn {
                self?.schoolDetail = schoolItem
                self?.reloadSchoolData?()
            }
        }
        if self?.schoolDetail.dbn == nil {
            self?.showAlertView?()
        }
    }
    .store(in: &cancellables)
    }
}

