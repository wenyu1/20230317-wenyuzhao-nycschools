//
//  SchoolViewController.swift
//  20230317-WenyuZhao-NYCSchools
//
//  Created by Wenyu Zhao on 3/17/23.
//

import UIKit
import Combine

class SchoolViewController: UIViewController {
    
    @IBOutlet weak var schoolsTableView: UITableView!
    lazy var schoolViewModel = {
        SchoolsViewModel()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        initializeSchoolList()
    }
    
    func initializeSchoolList() {
        schoolViewModel.getSchoolData()
        self.title = "NYC Schools"
        // Reloading the School Table View after API response
        schoolViewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.schoolsTableView.reloadData()
            }
        }
        schoolsTableView.delegate = self
        schoolsTableView.dataSource = self
    }
}

// Design the cell
extension SchoolViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolViewModel.schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let schoolCell = tableView.dequeueReusableCell(withIdentifier: "schoolCell", for: indexPath) as! SchoolListTableViewCell
        schoolCell.logoLabel.text = schoolViewModel.schools[indexPath.row].boro
        schoolCell.schoolName.text = schoolViewModel.schools[indexPath.row].school_name
        schoolCell.schoolWebsite.text = schoolViewModel.schools[indexPath.row].website
        return schoolCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SchoolDetailViewController") as? SchoolDetailViewController
        if let dbn = schoolViewModel.schools[indexPath.row].dbn {
            vc?.schoolDetailViewModel.dbn = dbn
            self.navigationController?.pushViewController(vc!, animated: true)
        }
        
    }
}

