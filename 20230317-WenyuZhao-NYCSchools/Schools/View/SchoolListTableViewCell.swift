//
//  SchoolListTableViewCell.swift
//  20230317-WenyuZhao-NYCSchools
//
//  Created by Wenyu Zhao on 3/17/23.
//

import UIKit

class SchoolListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var logoLabel: UILabel!
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolWebsite: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.logoLabel.layer.cornerRadius = logoLabel.frame.width/4
        self.logoLabel.layer.borderWidth = 1.2
        self.logoLabel.layer.borderColor = UIColor.brown.cgColor
        self.logoLabel.backgroundColor = .blue.withAlphaComponent(0.5)
        self.logoLabel.clipsToBounds = true
    }
}
