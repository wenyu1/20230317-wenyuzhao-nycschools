//
//  SchoolsViewModel.swift
//  20230317-WenyuZhao-NYCSchools
//
//  Created by Wenyu Zhao on 3/17/23.
//

import Foundation
import Combine

class SchoolsViewModel: NSObject {
    
    private var cancellables = Set<AnyCancellable>()
    var reloadTableView: (() -> Void)?
    var schools = Schools() {
        didSet {
            reloadTableView?()
        }
    }
    
    // Retrive data from URL
    // If there is more time, I will build sort and filter buttons for the table view.
    func getSchoolData() {
        let schoolsUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        NetworkManager.shared.getData(url: schoolsUrl, type: School.self)
            .sink { completion in
                switch completion {
                case .failure(let err):
                    print("Error is \(err.localizedDescription)")
                case .finished:
                    print("Finished")
                }
            }
    receiveValue: { [weak self] usersData in
        self?.schools = usersData
    }
    .store(in: &cancellables)
    }
}

